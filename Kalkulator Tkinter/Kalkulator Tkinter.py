import tkinter as tk

root = tk.Tk() #tworzenie okna

e = tk.Entry(root, width= 50)
e.grid(row=0, column = 0, columnspan = 3, padx= 40, pady = 40)

def cyfra(cyfra):
    wPasku = e.get()
    e.delete(0, tk.END)
    e.insert(0, str(wPasku) +str(cyfra))

def suma():
    liczba1 = e.get()
    global pierwszaLiczba
    global operacja
    operacja = 'dodaj'
    pierwszaLiczba = int(liczba1)
    e.delete(0, tk.END)

def roznica():
    liczba1 = e.get()
    global pierwszaLiczba
    global operacja
    operacja = 'odejmij'
    pierwszaLiczba = int(liczba1)
    e.delete(0, tk.END)

def iloczyn():
    liczba1 = e.get()
    global pierwszaLiczba
    global operacja
    operacja = 'pomnoz'
    pierwszaLiczba = int(liczba1)
    e.delete(0, tk.END)

def iloraz():
    liczba1 = e.get()
    global pierwszaLiczba
    global operacja
    operacja = 'podziel'
    pierwszaLiczba = int(liczba1)
    e.delete(0, tk.END)

def rownaSie():
    liczba2 = e.get()
    e.delete(0, tk.END)
    if operacja == 'dodaj':
        e.insert(0, pierwszaLiczba + int(liczba2))
    elif operacja == 'odejmij':
        e.insert(0, pierwszaLiczba - int(liczba2))
    elif operacja == 'pomnoz':
        e.insert(0, pierwszaLiczba * int(liczba2))
    elif operacja == 'podziel':
        e.insert(0, int(pierwszaLiczba / int(liczba2)))

def c():
    e.delete(0, tk.END)

def naBinarny():
    liczba = e.get()
    liczba = bin(int(liczba))[2:]
    e.delete(0, tk.END)
    e.insert(0, liczba)

def naDecymalny():
    liczba = e.get()
    liczba = int(liczba, 2)
    e.delete(0, tk.END)
    e.insert(0, liczba)

przycisk1 = tk.Button(root, text='1', padx= 30, pady = 30, command = lambda: cyfra(1))
przycisk2 = tk.Button(root, text='2', padx= 30, pady = 30, command = lambda: cyfra(2))
przycisk3 = tk.Button(root, text='3', padx= 30, pady = 30, command = lambda: cyfra(3))
przycisk4 = tk.Button(root, text='4', padx= 30, pady = 30, command = lambda: cyfra(4))
przycisk5 = tk.Button(root, text='5', padx= 30, pady = 30, command = lambda: cyfra(5))
przycisk6 = tk.Button(root, text='6', padx= 30, pady = 30, command = lambda: cyfra(6))
przycisk7 = tk.Button(root, text='7', padx= 30, pady = 30, command = lambda: cyfra(7))
przycisk8 = tk.Button(root, text='8', padx= 30, pady = 30, command = lambda: cyfra(8))
przycisk9 = tk.Button(root, text='9', padx= 30, pady = 30, command = lambda: cyfra(9))
przycisk0 = tk.Button(root, text='0', padx= 30, pady = 30, command = lambda: cyfra(0))
przyciskDodaj = tk.Button(root, text='+', padx = 30, pady = 30, command = suma)
przyciskOdejmij = tk.Button(root, text='-', padx = 30, pady = 30, command = roznica)
przyciskMnoz = tk.Button(root, text='*', padx = 30, pady = 30, command = iloczyn)
przyciskDziel = tk.Button(root, text='/', padx = 30, pady = 30, command = iloraz)
przyciskRownaSie = tk.Button(root, text='=', padx = 30, pady = 30, command = rownaSie)
przyciskC = tk.Button(root, text='C', padx = 30, pady = 30, command = c)
przyciskBinarny = tk.Button(root, text="na binarny", padx = 30, pady = 30, command = naBinarny)
przyciskDecymalny = tk.Button(root, text="na decymaly", padx = 30, pady = 30, command = naDecymalny)

przycisk1.grid(row=3, column=0)
przycisk2.grid(row=3, column=1)
przycisk3.grid(row=3, column=2)
przycisk4.grid(row=2, column=0)
przycisk5.grid(row=2, column=1)
przycisk6.grid(row=2, column=2)
przycisk7.grid(row=1, column=0)
przycisk8.grid(row=1, column=1)
przycisk9.grid(row=1, column=2)
przycisk0.grid(row=4, column=0)
przyciskDodaj.grid(row=1, column=3)
przyciskOdejmij.grid(row=2, column=3)
przyciskMnoz.grid(row=3, column=3)
przyciskDziel.grid(row=4, column=3)
przyciskRownaSie.grid(row=4, column=1)
przyciskC.grid(row=4, column=2)
przyciskBinarny.grid(row=5, column=1)
przyciskDecymalny.grid(row=5, column = 2)

root.mainloop() #wywolanie okna